#!/usr/bin/perl -w
use strict;
use open ':std', ':encoding(UTF-8)';
use WWW::Mechanize;

# This program was written by Henry Lao
# v1.1b June 2014

# regex revision

my $mech = WWW::Mechanize->new();
my $url = "";

my $no_of_pages_to_search = 150;

my @product_names = ();
my @product_id_codes = ();
my @product_id_prices = ();

my $source_code = ();
$| = 1;

print <<INFOHEADER;
This program will pull every in-store clearance deal from the Source's
website and put them into a single spreadsheet.

Press 'Enter' to begin.
INFOHEADER

<STDIN>;

print "Percent parsed:\n";

open my $fh, ">","list.csv" or die $!;
print $fh "ITEM CODE, ITEM NAME, PRICE, LINK\n";

for (my $i = 1; $i < $no_of_pages_to_search; $i++) {

    $url = "http://www.thesource.ca/estore/category.aspx?language=en-CA&catalog=Online&category=Clearance+Items&sort=1&pagenum=$i";
    $mech ->get($url);
    $source_code = $mech->content;

    while ($source_code =~ /ProductName" class="MenuText" href=.+?>(.+?)<\/a>.+?CatalogNumber" class="MenuText">(.+?)<\/span>.+?itemprop="price">(.+?)<\/span>/gs) {
	my $itemname = $1;
        $itemname =~ tr/,/ /;
        push @product_names, $itemname;

	push @product_id_codes, $2;

        my $itemprices = $3;
        $itemprices =~ /(\$[0-9]*\.[0-9]*)/;
        push @product_id_prices, $1;
    }

    my $percent = sprintf ("%05.2f", $i*100/$no_of_pages_to_search);
    print "$percent %\r";
}

my $product_link_skele = "http://www.thesource.ca/estore/Product.aspx?language=en-CA&catalog=Online&category=Clearance+Items&product=";

for (my $j=0; $j<scalar @product_names; $j++) {
    print $fh "$product_id_codes[$j], $product_names[$j], $product_id_prices[$j], $product_link_skele$product_id_codes[$j]\n";
}

print "\n\nProgram complete!\nPress 'Enter' to exit.\n";
<STDIN>;
close $fh;