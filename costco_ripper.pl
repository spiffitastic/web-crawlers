#!/usr/bin/perl -w
use strict;
use open ':std', ':encoding(UTF-8)';
use WWW::Mechanize;

my $mech = WWW::Mechanize->new();
my $url = "http://www.costco.ca/SiteMap?storeId=10302&catalogId=11201";
$mech ->get($url);
my $source_code = $mech->response->decoded_content;

my @link_list = ();

while ($source_code =~ /<li class="subcategory"><a href="(.+?)">(.+?)<\/a><\/li>/gis) {
    my $link = $1;
    push @link_list, $link;
}
# extract every link from the sitemap

print "Decoded Costco sitemap.\nAll relevent URLs obtained.\n\n";

my @item_names;
my @item_prices;
my @item_links;
my @item_codes;

$| = 1;
my $counter = 1;
my $page_count = scalar @link_list;
# for the progress counter later

my $regex = '<div class="scProdId" sc.prodId="(.+?)"(.+?)product-tile-image-container(.+?)<a href="(.+?)"(.+?)<span class="short-desc">(.+?)<\/span>((.+?)<span data-regionNav="DEFAULT">(.+?)<\/span>)?';

foreach (@link_list) {
    my $url2 = $_;
    my $mech2 = WWW::Mechanize->new();
    
    $mech2 ->get($url2);
    my $source_code2 = $mech2->content;
    chomp $source_code2;
    
    while ($source_code2 =~ /$regex/gis) {
        
        my $item_code = $1; 
        push @item_codes, $item_code;
        
        my $item_link = $4; 
        push @item_links, $item_link;
        
        my $item_name = $6; 
        push @item_names, $item_name;
        
        my $item_price;
        if ($9) {
            $item_price = $9;
        } else {
            $item_price = 'not available';
        }
        push @item_prices, $item_price;
        # not all items have a price, esp custom orders -> in which case, return 'null'
    }
    
    $counter = $counter + 1;
    print "Pages processed: $counter out of $page_count.\r";
}

foreach (@item_names, @item_prices) {
    $_ =~ s/,//g;
}

open my $fh, ">","list.csv" or die $!;
print $fh "ITEM CODE, ITEM NAME, PRICE, LINK\n";

for (my $j=0; $j<scalar @item_names; $j++) {
    print $fh "$item_codes[$j], $item_names[$j], $item_prices[$j], $item_links[$j]\n";
}

close $fh;

print "\n\nDone!\n";

<STDIN>;