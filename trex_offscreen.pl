#!/usr/bin/perl -w
use strict;
use WWW::Mechanize;
use utf8;

print <<INFOHEADER;
This program will pull every device from gfxbench's
T-Rex 1080p Offscreen benchmark results list.

Press 'Enter' to begin.
INFOHEADER

<STDIN>;

my $mech = WWW::Mechanize->new();
my $url = "http://gfxbench.com/result.jsp?benchmark=gfx30&data-source=1&version=all&test=547&text-filter=&order=median&os-Android_gl=true&os-iOS_gl=true&arch-ARM=true&arch-unknown=true&arch-x86=true&base=device";
$mech ->get($url);
my $source_code = $mech->content;
chomp $source_code;

$source_code =~/var firstResult(.+?)\[.(.+?)..\]/;
my $firstresult_uf = $2;
my @firstresult_array = split /','/, $firstresult_uf;
# date

$source_code =~/var deviceName(.+?)\[.(.+?)..\]/;
my $deviceName_uf = $2;
$deviceName_uf =~ s/\((.+?)\)//g;
my @deviceName_array = split /','/, $deviceName_uf;
# device_name, some names have commas, so the regex is a little funky

$source_code =~/var os(.+?)\[(.+?)\]/;
my $opsys_uf = $2;
my @opsys_array = split /,/, $opsys_uf;
# opsys - droid(1), linux(3), ios(2), windows(0)

foreach (@opsys_array) {
    if ($_==0) {
        $_ = 'Android';
    } elsif ($_==1) {
        $_ = 'iOS/X';
    }
}

$source_code =~/var arch(.+?)\[(.+?)\]/;
my $arch_uf = $2;
my @arch_array = split /,/, $arch_uf;
# archi - mips(2), x86(0), arm(1)

foreach (@arch_array) {
    if ($_==0) {
        $_ = 'ARM'
    } elsif ($_==1) {
        $_ = 'x86'
    } elsif ($_==2) {
        $_ = 'Unknown'
    }
}

$source_code =~/var score(.+?)\[(.+?)\]/;
my $score_uf = $2;
my @score_array = split /,/, $score_uf;
#top score rawr

$source_code =~/var score_b(.+?)\[(.+?)\]/;
my $score_b_uf = $2;
my @score_b_array = split /,/, $score_b_uf;
# avg score raw

open my $fh, ">","gfxbench_trex_1080p.csv" or die $!;
print $fh "DEVICE,POSTED,OP SYS,ARCH,AVG SCORE,TOP SCORE\n";
for (my $x=0; $x<scalar @firstresult_array; $x++) {
    print $fh "$deviceName_array[$x],$firstresult_array[$x],$opsys_array[$x],$arch_array[$x],$score_b_array[$x],$score_array[$x]\n";
}
close $fh;

print "DONE!\n\nPress 'Enter' to exit.\n";
<STDIN>;